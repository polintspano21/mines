﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSwipper
{
    public partial class History : Form
    {
        public History()
        {
            InitializeComponent();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            try
            {
                dgv_History.Rows.Clear();
                List<string> lines = new List<string>();
                StreamReader reader = new StreamReader("Results/test.txt");

                using (reader)
                {
                    string line = reader.ReadLine();
                    while (line != null)
                    {
                        lines.Add(line);
                        line = reader.ReadLine();
                    }
                }
                for (int i = 0; i < lines.Count; i += 5)
                {
                    dgv_History.Rows.Add(lines[i], lines[i + 1], lines[i + 2], lines[i + 3], lines[i + 4]);
                }
            }
            catch (ArgumentOutOfRangeException)
            { 
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
