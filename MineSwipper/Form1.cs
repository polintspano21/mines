﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MineSwipper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Shema[,] array_model;
        int[,] position_of_elements; // positionOfElement - pascal  caase
        int opiti = 0;
        int level;
        int max_matrix = 0;
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tbUserName.Text == "")
            {
                MessageBox.Show("Моля въведете име на играча");
                return;
            }
            panel1.Enabled = true;
            System.GC.Collect();
            int max = 0;
            if (comboBox1.SelectedIndex == 0)
            {
                max = 5;
            }
            else if (comboBox1.SelectedIndex == 1)
            {
                max = 10;
            }
            else
            {
                max = 20;
            }
            max_matrix = max;
            GenerateAray(max);
            array_model = new Shema[max, max];
            position_of_elements = new int[max, max];
            EnterNUmbersInMatrix(max);
            EnterBOmbs(max, max);
        }

        private void EnterNUmbersInMatrix(int max)
        {
            int number = 1;
            for (int i = 0; i < max; i++)
            {
                for (int j = 0; j < max; j++)
                {
                    position_of_elements[i, j] = number;
                    number++;
                }
            }
        }

        private void EnterBOmbs(int max, int limit)
        {
            Random rand = new Random();
            for (int i = 0; i < array_model.GetLength(0); i++)
            {
                for (int j = 0; j < array_model.GetLength(0); j++)
                {
                    array_model[i, j] = new Shema(0, 0, false);
                }
            }
            for (int i = 0; i < max; i++)
            {
                int x = rand.Next(0, limit);
                int y = rand.Next(0, limit);
                int number = rand.Next(0, 51);
                if (array_model[x, y].bomb == true)
                {
                    max++;
                }
                else
                {
                    array_model[x, y].bomb = true;
                    CalcualteINdiucators(x, y, limit);
                }
            }
        }

        private void CalcualteINdiucators(int x, int y, int limit)
        {
            if (x + 1 < limit)
            {
                array_model[x + 1, y].count_bombs++;
            }
            if (x + 1 < limit && y + 1 < limit)
            {
                array_model[x + 1, y + 1].count_bombs++;
            }
            if (y + 1 < limit)
            {
                array_model[x, y + 1].count_bombs++;
            }

            if (x - 1 > -1)
            {
                array_model[x - 1, y].count_bombs++;

            }
            if (x - 1 > -1 && y - 1 > -1)
            {
                array_model[x - 1, y - 1].count_bombs++;

            }
            if (y - 1 > -1)
            {
                array_model[x, y - 1].count_bombs++;
            }
            if (y - 1 > -1 && x + 1 < limit)
            {
                array_model[x + 1, y - 1].count_bombs++;
            }
            if (x - 1 > -1 && y + 1 < limit)
            {
                array_model[x - 1, y + 1].count_bombs++;
            }

        }

        private void GenerateAray(int max)
        {
            panel1.Controls.Clear();
            int x = 0;
            int y = 0;
            for (int i = 0; i < (max * max); i++)
            {
                Button button = new Button();
                button.Name = (i + 1).ToString();
                button.Size = new Size(20, 20);
                button.Location = new Point(x, y);
                button.Click += new EventHandler(CHeckPOsition);
                panel1.Controls.Add(button);
                x += 20;
                if (i == 0)
                {
                    continue;
                }
                if ((i + 1) % max == 0)
                {
                    x = 0;
                    y += 20;
                }
            }
        }

        private bool FinishGame()
        {
            if (GetTotalPOsition())
            {
                WinnerPlayer();
                return true;
            }
            return false;
        }

        public void Winner(string winner)
        {
            MessageBox.Show(winner);
            AddInfoInFile(winner, level);
            DialogResult dialog = MessageBox.Show(winner + "is winner" + "in level" + level + "Do you want another game?", "Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                comboBox1_SelectedIndexChanged(null, null);
            }
            else
            {
                panel1.Enabled = false;
            }
        }

        private void WinnerPlayer()
        {
            Winner(tbUserName.Text);
        }

        private bool GetTotalPOsition()
        {
            int count_max = 0;
            foreach (var item in panel1.Controls.OfType<Button>())
            {
                if (item.Enabled == true)
                {
                    count_max++;
                }
            }
            if (count_max == max_matrix)
            {
                return true;
            }
            return false;
        }

        private void AddInfoInFile(string winner, int level)
        {
            using (System.IO.StreamWriter stream = new StreamWriter("Results/test.txt", true, Encoding.GetEncoding("Windows-1251")))
            {
                stream.WriteLine("0");
                stream.WriteLine(DateTime.Now.ToString("dd.MM.yyyy"));
                stream.WriteLine(DateTime.Now.ToString("HH.mm.ss"));
                stream.WriteLine(winner);
                stream.WriteLine(level.ToString());
            }
        }

        private void CHeckPOsition(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (CHeckWeHAveBombAtPosition(int.Parse(button.Name)))
            {
                FinishGameLose();
                return;
            }
            if (FinishGame())
            {
                return;
            }
            GetPOsition(button);

            button.Enabled = false;
        }

        private void FinishGameLose()
        {
            List<int> position_bombs = new List<int>();
            for (int i = 0; i < max_matrix; i++)
            {
                for (int j = 0; j < max_matrix; j++)
                {
                    if (array_model[i, j].bomb == true)
                    {
                        position_bombs.Add(position_of_elements[i, j]);
                    }
                }
            }
            foreach (var item in panel1.Controls.OfType<Button>())
            {
                foreach (var item_position in position_bombs)
                {
                    if (item.Name == item_position.ToString())
                    {
                        item.BackColor = Color.Red;
                    }
                    item.Enabled = false;
                }
            }
            Winner("Game");
        }

        private void GetPOsition(Button button)
        {
            for (int i = 0; i < position_of_elements.GetLength(0); i++)
            {
                for (int j = 0; j < position_of_elements.GetLength(1); j++)
                {
                    if (int.Parse(button.Name) == position_of_elements[i, j])
                    {
                        if (array_model[i, j].count_bombs != 0)
                        {
                            button.Enabled = false;
                            button.Text = array_model[i, j].count_bombs.ToString();
                            return;
                        }
                        Game(i, j, int.Parse(button.Name), position_of_elements.GetLength(0));
                        break;
                    }
                }
            }
        }

        private bool CHeckWeHAveBombAtPosition(int number)
        {
            for (int i = 0; i < position_of_elements.GetLength(0); i++)
            {
                for (int j = 0; j < position_of_elements.GetLength(1); j++)
                {
                    if (number == position_of_elements[i, j])
                    {
                        if (array_model[i, j].bomb == true)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        List<int> number_list = new List<int>();
        List<int> freePOsition = new List<int>();
        private void Game(int x, int y, int numberOfBUtton, int limit)
        {
            freePOsition = new List<int>();
            number_list = new List<int>();
            GetLeftButtomObject(x, y, limit);
            GetRightBUttomtObject(x, y, limit);
            GetRightToptObject(x, y, limit);
            GetLeftTopObject(x, y, limit);
            foreach (var item_list in number_list)
            {
                foreach (Button item in panel1.Controls.OfType<Button>())
                {
                    if (int.Parse(item.Name) == item_list)
                    {
                        if (CHeckPOsition(int.Parse(item.Name)))
                        {
                            item.Enabled = true;
                        }
                        item.Enabled = false;
                        item.Text = GetCountOFBOmbs(item_list);
                        switch (int.Parse(item.Text))
                        {
                            case 1: item.ForeColor = Color.Green;break;
                            case 2: item.ForeColor = Color.Yellow; break;
                            case 3: item.ForeColor = Color.Blue; break;
                            default:
                                item.ForeColor = Color.Red;
                                break;
                        }
                    }
                }
            }
            foreach (var item_list in freePOsition)
            {
                foreach (Button item in panel1.Controls.OfType<Button>())
                {
                    if (int.Parse(item.Name) == item_list)
                    {
                        item.Enabled = false;
                    }
                }
            }

        }

        private void GetLeftTopObject(int x, int y, int limit)
        {
            if (y < 0)
            {
                return;
            }
            int temp_y = -1;
            int position = 0;
            int chilfposition = 0;
            for (int i = x; i > -1; i--)
            {
                if (array_model[i, y].bomb == true)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                if (array_model[i, y].count_bombs != 0)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                freePOsition.Add(position_of_elements[i, y]);
                if (i - 1 >-1)
                {
                    if (array_model[i -1, y].bomb == false || array_model[i -1, y].count_bombs == 0)
                    {
                        temp_y = y -1;
                    }
                    else if (array_model[i + 1, y].count_bombs == 0)
                    {
                        number_list.Add(position_of_elements[i -1, y -1]);
                    }
                }
            }
            if (temp_y != -1)
            {
                GetLeftTopObject(x, temp_y, limit);
            }
        }

        private void GetRightToptObject(int x, int y, int limit)
        {
            if (y == limit)
            {
                return;
            }
            int temp_y = -1;
            int position = 0;
            int chilfposition = 0;
            for (int i = x; i > -1; i--)
            {
                if (array_model[i, y].bomb == true)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                if (array_model[i, y].count_bombs != 0)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                freePOsition.Add(position_of_elements[i, y]);
                if (i + 1 < limit)
                {
                    if (array_model[i + 1, y].bomb == false || array_model[i + 1, y].count_bombs == 0)
                    {
                        temp_y = y + 1;
                    }
                    else if (array_model[i + 1, y].count_bombs == 0)
                    {
                        number_list.Add(position_of_elements[i + 1, y + 1]);
                    }
                }
            }
            if (temp_y != -1)
            {
                GetRightToptObject(x, temp_y, limit);
            }
        }

        private void GetRightBUttomtObject(int x, int y, int limit)
        {
            if (y == limit)
            {
                return;
            }
            int temp_y = -1;
            int position = 0;
            int chilfposition = 0;
            for (int i = x; i < limit; i++)
            {
                if (array_model[i, y].bomb == true)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                if (array_model[i, y].count_bombs != 0)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                freePOsition.Add(position_of_elements[i, y]);
                if (i +1 < limit)
                {
                    if (array_model[i + 1, y].bomb == false || array_model[i + 1, y].count_bombs == 0)
                    {
                        temp_y = y + 1;
                    }
                    else if (array_model[i + 1, y].count_bombs == 0)
                    {
                        number_list.Add(position_of_elements[i + 1, y  +1]);
                    }
                }
            }
            if (temp_y != -1)
            {
                GetRightBUttomtObject(x, temp_y, limit);
            }
        }

        private void GetLeftButtomObject(int x, int y, int limit)
        {
            if (y < 0)
            {
                return;
            }
            int temp_y = -1;
            int position = 0;
            int chilfposition = 0;
            for (int i = x; i < limit; i++)
            {
                if (array_model[i, y].bomb == true)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                if (array_model[i, y].count_bombs != 0)
                {
                    number_list.Add(position_of_elements[i, y]);
                    break;
                }
                freePOsition.Add(position_of_elements[i , y]);
                if (i - 1 > -1)
                {
                    if (array_model[i-1, y].bomb == false || array_model[i-1, y].count_bombs == 0)
                    {
                        temp_y = y - 1;
                    }
                    else if (array_model[i - 1, y].count_bombs == 0)
                    {
                        number_list.Add(position_of_elements[i - 1, y -1]);
                    }
                }
            }
            if (temp_y != -1)
            {
                GetLeftButtomObject(x, temp_y, limit);
            }
        }

        private bool CHeckPOsition(int v)
        {
            for (int i = 0; i < position_of_elements.GetLength(0); i++)
            {
                for (int j = 0; j < position_of_elements.GetLength(1); j++)
                {
                    if (v == position_of_elements[i, j] && array_model[i, j].bomb == true)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
     
        private string GetCountOFBOmbs(int item_list)
        {
            for (int i = 0; i < position_of_elements.GetLength(0); i++)
            {
                for (int j = 0; j < position_of_elements.GetLength(1); j++)
                {
                    if (item_list == position_of_elements[i, j])
                    {
                        if (array_model[i, j].count_bombs > 0)
                        {
                            return array_model[i, j].count_bombs.ToString();
                        }
                        else
                        {
                            return "";
                        }
                    }
                }
            }
            return "";
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            History model = new History();
            model.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        } 
    }
}
